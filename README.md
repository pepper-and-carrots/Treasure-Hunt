# Treasure Hunt

Pepper and Carrot treasure hunt is very simple game that can be played indoors with kids, the game consists of 16 clue cards all of them with pictures so it can be played by kids that don't know how to read.
Original from Filipe Vieira (https://framagit.org/peppercarrot/scenarios/-/issues/6)
